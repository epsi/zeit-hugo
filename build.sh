#!/usr/bin/env bash

curl -L -O https://github.com/gohugoio/hugo/releases/download/v0.62.2/hugo_0.62.2_Linux-64bit.tar.gz
tar -xzf hugo_0.62.2_Linux-64bit.tar.gz

./hugo
